defmodule Mumbling do
  def accum(s) do
    s
    |> String.split("", trim: true)
    |> Stream.with_index(1)
    |> Enum.map(fn {s, i} -> String.duplicate(s, i) end)
    |> Enum.map(fn s -> String.capitalize(s) end)
    |> Enum.join("-")
  end
end


IO.inspect(Mumbling.accum("ZpglnRxqenU"))